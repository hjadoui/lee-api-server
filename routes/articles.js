var express = require('express');
var router = express.Router();
var Article = require('../models/Article.js');
var User = require('../models/User.js');

var passport = require('passport');
require('../config/passport')(passport);






router.get('/', function(req, res, next) {

    data = "";
    if(req.query.search){
        data = req.query.search
    }

    var regex = new RegExp(data, "i")

    Article.find({content : regex},function (err, articles) {
        if (err) console.log(err);

        res.json(articles);
    });
});


router.post('/', passport.authenticate('jwt', { session: false}), function(req, res) {

    var user = req.user;
    var article = new Article({ title: req.body.title,content:req.body.article,category:req.body.category ,author : user._id});
    console.log(article);
    user.articles.push(article);

    user.save(function (err) {
        if (!err) {
            console.log(user);
        }
    });
    article.save(function (err) {
        if (!err) {
            console.log(user);
            res.json({test :'test ok'});
        }
    });


});


module.exports = router;