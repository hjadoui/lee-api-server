var express = require('express');
var router = express.Router();
var User = require('../models/User.js');
var Article = require('../models/Article.js');
var userService = require('../services/user-service')

var passport = require('passport');
require('../config/passport')(passport);



router.post('/test', function(req, res) {
    console.log(req.body);
    console.log("ok");
    res.json("test okk");

});

/* GET ALL users */



/* SAVE user
router.post('/', function(req, res, next) {
    User.create(req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});
*/

router.post('/', function(req, res, next) {
    User.create(req.body, function (err, post) {
        if (err) res.json({success: true, msg: 'sign in failed'})
        res.json({success: true, msg: 'sign up successfully.'});
    });
});

router.post('/login', function(req, res) {
    User.findOne({
        pseudo: req.body.pseudo
    }).then(user => {
        userService.login(user,req)
            .then(data => {
                console.log(data)
                res.json(data)
            })
            .catch(err => {
                console.log("errr"+err)
                res.json(err)
            })
        })
        .catch(err => {throw err;})
});

router.get('/profile', passport.authenticate('jwt', { session: false}), function(req, res) {


    Article.find({
        _id : { $in: req.user.articles}
    }).then(result => {
        res.json({pseudo : req.user.pseudo, email:req.user.email,articles:result});
    })
        .catch(err => {throw err;})

});

router.get('/logout', passport.authenticate('jwt', { session: false}), function(req, res) {
    req.logout();
    res.json({success: true, msg: 'log out successfully.'});
});


/* GET SINGLE user BY ID*/
router.get('/:id', function(req, res, next) {
    User.findById(req.params.id, function (err, post) {
        if (err) return next(err);
        res.json({pseudo : post.pseudo});
    });
});


/*
/* UPDATE PRODUCT
router.put('/:id', function(req, res, next) {
    Product.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* DELETE PRODUCT
router.delete('/:id', function(req, res, next) {
    Product.findByIdAndRemove(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});
*/
module.exports = router;