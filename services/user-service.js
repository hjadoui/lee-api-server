var jwt = require('jsonwebtoken');
var config = require('../config/database');

module.exports = {
    login: function(user,req) {
        return new Promise(function(resolve, reject) {

            if (!user) {
                reject({success: false, msg: 'Authentication failed. User not found.'})
            } else {
                // check if password matches
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                        // if user is found and password is right create a token
                        const token = jwt.sign(user.toJSON(), config.secret, {
                            expiresIn: 604800 // 1 week
                        });
                        // return the information including token as JSON
                        resolve({success: true, token: 'JWT ' + token});
                    } else {
                        reject({success: false, msg: 'Authentication failed. User not found.'})
                    }
                });
            }
        });
    },

    getToken : function (headers) {
        if (headers && headers.authorization) {
            var parted = headers.authorization.split(' ');
            if (parted.length === 2) {
                return parted[1];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

};