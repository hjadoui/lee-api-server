var cors = require('cors')
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var passport = require('passport');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);




mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/articlesDB',{ useNewUrlParser: true })
    .then(() =>  console.log('connection succesful'))
    .catch((err) => console.error(err));


var db = mongoose.connection;



var users = require('./routes/users');
var articles = require('./routes/articles');


var app = express();

app.use(passport.initialize());
app.use(cookieParser());
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

//use sessions for tracking logins
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: false
}))

app.use('/users', users);
app.use('/articles', articles);

module.exports = app;
