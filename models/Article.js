var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var User = require('./User.js')

var UserSchema = new mongoose.Schema({
    title: String,
    category: String,
    content: String,
    author: { type:mongoose.Schema.Types.ObjectId, ref: 'User' },
});


var User = mongoose.model('Article', UserSchema);
module.exports = User;