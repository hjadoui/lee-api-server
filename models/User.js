var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Article = require('./Article.js')


var UserSchema = new mongoose.Schema({
    pseudo: String,
    email: String,
    articles : [{type:mongoose.Schema.Types.ObjectId, ref:'Article'}],
    password: String,
});

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});


UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', UserSchema);
module.exports = User;