# Api server
This project was generated with express for Nodejs

## Requires

1-Nodejs

    sudo apt update
    sudo apt install nodejs
    sudo apt install npm
    
2-mongodb

    sudo apt install mongodb-server
    sudo service mongodb start
    
## Install project

    npm install
    
## Run project 

    npm run start
    
Or
    
    npm run start:watch